package com.patrykel.rnrsapp;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.patrykel.rnrsapp.domain.Activity;
import com.patrykel.rnrsapp.domain.ActivityType;
import com.patrykel.rnrsapp.service.ActivityService;
import com.patrykel.rnrsapp.web.ActivityRestController;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Created by patryklawski on 1/22/18.
 */

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class ActivityControllerUnitTest {

  private MockMvc mockMvc;

  @Mock
  private ActivityService activityService;

  @InjectMocks
  private ActivityRestController activityRestController;

  @Before
  public void init() {
    MockitoAnnotations.initMocks(this);
    mockMvc = MockMvcBuilders
        .standaloneSetup(activityRestController)
        .build();
  }

  // =========================================== Get All Activities ==========================================

  @Test
  public void test_get_all_success() throws Exception {
    Date start = new Date();
    Date stop = new Date();

    List<Activity> activities = Arrays.asList(
        new Activity(1L, 1L, start, stop, ActivityType.EATING),
        new Activity(2L, 2L, start, stop, ActivityType.RELAX)
    );

    when(activityService.getAll()).thenReturn(activities);

    mockMvc.perform(get("/activity/all"))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$", hasSize(2)))

        .andExpect(jsonPath("$[0].id", is(1)))
        .andExpect(jsonPath("$[0].userId", is(1)))
        .andExpect(jsonPath("$[0].start", is(start.getTime())))
        .andExpect(jsonPath("$[0].stop", is(stop.getTime())))
        .andExpect(jsonPath("$[0].activityType", is(ActivityType.EATING.toString())))

        .andExpect(jsonPath("$[1].id", is(2)))
        .andExpect(jsonPath("$[1].userId", is(2)))
        .andExpect(jsonPath("$[1].start", is(start.getTime())))
        .andExpect(jsonPath("$[1].stop", is(stop.getTime())))
        .andExpect(jsonPath("$[1].activityType", is(ActivityType.RELAX.toString())));

    verify(activityService, times(1)).getAll();
    verifyNoMoreInteractions(activityService);
  }


  // =========================================== Get Activity By UserID =========================================

  @Test
  public void test_get_by_userId_success() throws Exception {
    Date start = new Date();
    Date stop = new Date();

    List<Activity> activities = Arrays.asList(
        new Activity(1L, 1L, start, stop, ActivityType.EATING)
    );

    when(activityService.getAll(1L)).thenReturn(activities);

    mockMvc.perform(get("/activity/user/{userId}", 1))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$", hasSize(1)))
        .andExpect(jsonPath("$[0].id", is(1)))
        .andExpect(jsonPath("$[0].userId", is(1)))
        .andExpect(jsonPath("$[0].start", is(start.getTime())))
        .andExpect(jsonPath("$[0].stop", is(stop.getTime())))
        .andExpect(jsonPath("$[0].activityType", is(ActivityType.EATING.toString())));

    verify(activityService, times(1)).getAll(1L);
    verifyNoMoreInteractions(activityService);
  }

  @Test
  public void test_get_by_id_fail_404_not_found() throws Exception {
    when(activityService.getAll(3L)).thenReturn(null);

    mockMvc.perform(get("/activity/user/{userId}", 3))
        .andExpect(status().isOk());

    verify(activityService, times(1)).getAll(3L);
    verifyNoMoreInteractions(activityService);
  }

  // =========================================== Get Activity By UserID and Activity Type ==============
  @Test
  public void test_get_by_userId_and_activityType_success() throws Exception {
    Date start = new Date();
    Date stop = new Date();

    List<Activity> activities = Arrays.asList(
        new Activity(1L, 1L, start, stop, ActivityType.EATING),
        new Activity(2L, 1L, start, stop, ActivityType.EATING)
    );

    when(activityService.getAll(1L, ActivityType.EATING)).thenReturn(activities);

    mockMvc.perform(get("/activity/user/{userId}/{activityType}", 1, ActivityType.EATING.toString()))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$", hasSize(2)))
        .andExpect(jsonPath("$[0].id", is(1)))
        .andExpect(jsonPath("$[0].userId", is(1)))
        .andExpect(jsonPath("$[0].start", is(start.getTime())))
        .andExpect(jsonPath("$[0].stop", is(stop.getTime())))
        .andExpect(jsonPath("$[0].activityType", is(ActivityType.EATING.toString())))
        .andExpect(jsonPath("$[1].id", is(2)))
        .andExpect(jsonPath("$[1].userId", is(1)))
        .andExpect(jsonPath("$[1].start", is(start.getTime())))
        .andExpect(jsonPath("$[1].stop", is(stop.getTime())))
        .andExpect(jsonPath("$[1].activityType", is(ActivityType.EATING.toString())));


    verify(activityService, times(1)).getAll(1L, ActivityType.EATING);
    verifyNoMoreInteractions(activityService);
  }

  @Test
  public void test_get_by_userId_and_activityType_fail_404_not_found() throws Exception {
    when(activityService.getAll(1L, ActivityType.EATING)).thenReturn(null);

    mockMvc.perform(get("/activity/user/{userId}/{activityType}", 1, ActivityType.EATING))
        .andExpect(status().isOk());

    verify(activityService, times(1)).getAll(1L, ActivityType.EATING);
    verifyNoMoreInteractions(activityService);
  }

  // =========================================== Create New Activity ========================================

  @Test
  public void test_create_activity_success() throws Exception {
    Date start = new Date();
    Date stop = new Date();

    Activity activity = new Activity(1L, 1L, start, stop, ActivityType.EATING);

    when(activityService.exists(activity)).thenReturn(false);
    doNothing().when(activityService).create(activity);

    mockMvc.perform(
        post("/activity/create")
            .contentType(MediaType.APPLICATION_JSON)
            .content(asJsonString(activity)))
        .andExpect(status().isCreated());

    verify(activityService, times(1)).create(activity);
    verify(activityService, times(1)).exists(activity);
    verifyNoMoreInteractions(activityService);
  }

  @Test
  public void test_create_activity_fail_404_not_found() throws Exception {
    Date start = new Date();
    Date stop = new Date();

    Activity activity = new Activity(1L, 1L, start, stop, ActivityType.EATING);

    when(activityService.exists(activity)).thenReturn(true);

    mockMvc.perform(
        post("/activity/create")
            .contentType(MediaType.APPLICATION_JSON)
            .content(asJsonString(activity)))
        .andExpect(status().isConflict());

    verify(activityService, times(1)).exists(activity);
    verifyNoMoreInteractions(activityService);
  }


  public static String asJsonString(final Object obj) {
    try {
      final ObjectMapper mapper = new ObjectMapper();
      return mapper.writeValueAsString(obj);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }
}