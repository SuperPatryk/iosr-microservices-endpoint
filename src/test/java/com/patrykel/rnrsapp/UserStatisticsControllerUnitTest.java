package com.patrykel.rnrsapp;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.patrykel.rnrsapp.domain.Activity;
import com.patrykel.rnrsapp.domain.ActivityType;
import com.patrykel.rnrsapp.domain.UserStatistics;
import com.patrykel.rnrsapp.service.UserStatisticsService;
import com.patrykel.rnrsapp.web.UserStatisticsController;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by patryklawski on 1/23/18.
 */
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class UserStatisticsControllerUnitTest {

  private MockMvc mockMvc;

  @Mock
  private UserStatisticsService userStatisticsService;

  @InjectMocks
  private UserStatisticsController userStatisticsRestController;

  @Before
  public void init() {
    MockitoAnnotations.initMocks(this);
    mockMvc = MockMvcBuilders
        .standaloneSetup(userStatisticsRestController)
        .build();
  }

  // =========================================== Get All Statistics ==========================================

  @Test
  public void test_get_all_success() throws Exception {

    Date date = new Date();
    List<UserStatistics> statistics = Arrays.asList(
        new UserStatistics(1L, 1L, date, ActivityType.EATING, 1L, 10L, 100L),
        new UserStatistics(2L, 2L, date, ActivityType.SLEEPING, 2L, 20L, 200L)
    );

    when(userStatisticsService.getAll()).thenReturn(statistics);

    mockMvc.perform(get("/user-statistics/all"))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
        .andExpect(jsonPath("$", hasSize(2)))

        .andExpect(jsonPath("$[0].id", is(1)))
        .andExpect(jsonPath("$[0].userId", is(1)))
        .andExpect(jsonPath("$[0].timestamp", is(date.getTime())))
        .andExpect(jsonPath("$[0].activityType", is(ActivityType.EATING.toString())))
        .andExpect(jsonPath("$[0].timeSpentDuringDay", is(1)))
        .andExpect(jsonPath("$[0].timeSpentDuringWeek", is(10)))
        .andExpect(jsonPath("$[0].timeSpentDuringMoth", is(100)))

        .andExpect(jsonPath("$[1].id", is(2)))
        .andExpect(jsonPath("$[1].userId", is(2)))
        .andExpect(jsonPath("$[1].timestamp", is(date.getTime())))
        .andExpect(jsonPath("$[1].activityType", is(ActivityType.SLEEPING.toString())))
        .andExpect(jsonPath("$[1].timeSpentDuringDay", is(2)))
        .andExpect(jsonPath("$[1].timeSpentDuringWeek", is(20)))
        .andExpect(jsonPath("$[1].timeSpentDuringMoth", is(200)));


    verify(userStatisticsService, times(1)).getAll();
    verifyNoMoreInteractions(userStatisticsService);
  }

  // =========================================== Create New Statistic ========================================

  @Test
  public void test_create_activity_success() throws Exception {

    Date date = new Date();
    UserStatistics userStatistics = new UserStatistics(1L, 1L, date, ActivityType.EATING, 1L, 10L, 100L);

    when(userStatisticsService.exists(userStatistics)).thenReturn(false);
    doNothing().when(userStatisticsService).create(userStatistics);

    mockMvc.perform(
        post("/user-statistics/create")
            .contentType(MediaType.APPLICATION_JSON)
            .content(asJsonString(userStatistics)))
        .andExpect(status().isCreated());

    verify(userStatisticsService, times(1)).create(userStatistics);
    verify(userStatisticsService, times(1)).exists(userStatistics);
    verifyNoMoreInteractions(userStatisticsService);
  }

  @Test
  public void test_create_activity_fail_404_not_found() throws Exception {

    Date date = new Date();
    UserStatistics userStatistics = new UserStatistics(1L, 1L, date, ActivityType.EATING, 1L, 10L, 100L);


    when(userStatisticsService.exists(userStatistics)).thenReturn(true);

    mockMvc.perform(
        post("/user-statistics/create")
            .contentType(MediaType.APPLICATION_JSON)
            .content(asJsonString(userStatistics)))
        .andExpect(status().isConflict());

    verify(userStatisticsService, times(1)).exists(userStatistics);
    verifyNoMoreInteractions(userStatisticsService);
  }


  public static String asJsonString(final Object obj) {
    try {
      final ObjectMapper mapper = new ObjectMapper();
      return mapper.writeValueAsString(obj);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }
  
}