package com.patrykel.rnrsapp.domain;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;
import java.util.Date;

/**
 * Created by patryklawski on 1/12/18.
 */
public interface UserStatisticsRepository extends JpaRepository<UserStatistics, Long> {
  UserStatistics findById(Long userId);
  Collection<UserStatistics> findUserStatisticsByUserId(Long userId);
  Collection<UserStatistics> findUserStatisticsByTimestampBetween(Date start, Date stop);
  Collection<UserStatistics> findUserStatisticsByUserIdAndActivityType(Long userId, ActivityType activityType);
  Collection<UserStatistics> findUserStatisticsByUserIdAndTimestampBetween(Long userId, Date start, Date stop);
  Collection<UserStatistics> findUserStatisticsByUserIdAndActivityTypeAndTimestampBetween(Long userId,
                                                                                           ActivityType activityType,
                                                                                           Date start,
                                                                                           Date stop);
}