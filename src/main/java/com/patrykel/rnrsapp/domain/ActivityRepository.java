package com.patrykel.rnrsapp.domain;

import org.springframework.data.jpa.repository.JpaRepository;

import javax.xml.crypto.Data;
import java.util.Collection;
import java.util.Date;

/**
 * Created by patryklawski on 12/29/17.
 */
public interface ActivityRepository extends JpaRepository<Activity, Long> {

  Collection<Activity> findActivitiesByUserId(Long userId);

  Activity findById(Long actifityId);

  Collection<Activity> findActivitiesByUserIdAndActivityType(Long userId, ActivityType activityType);

  Collection<Activity> findAllByStartGreaterThanEqualAndStopLessThanEqual(Date start, Date stop);

  Collection<Activity> findActivitiesByUserIdAndStartGreaterThanEqualAndStopLessThanEqual(Long userId,
                                                                                          Date start,
                                                                                          Date stop);

  Collection<Activity> findActivitiesByUserIdAndActivityTypeAndStartGreaterThanEqualAndStopLessThanEqual(Long userId,
                                                                                                         ActivityType activityType,
                                                                                                         Date start,
                                                                                                         Date stop);

}
