package com.patrykel.rnrsapp.domain;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Created by patryklawski on 12/29/17.
 */
public enum ActivityType {
  SLEEPING,
  EATING,
  WORKING,
  TRAVELLING,
  RELAX;

  private static final List<ActivityType> VALUES =
      Collections.unmodifiableList(Arrays.asList(values()));
  private static final int SIZE = VALUES.size();
  private static final Random RANDOM = new Random();

  public static ActivityType randomType(){
    return VALUES.get(RANDOM.nextInt(SIZE));
  }
}
