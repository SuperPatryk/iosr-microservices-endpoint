package com.patrykel.rnrsapp.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

/**
 * Created by patryklawski on 12/29/17.
 */
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Activity {
  @Id
  @GeneratedValue
  private Long id;
  private Long userId;
  private Date start;
  private Date stop;
  private ActivityType activityType;
}
