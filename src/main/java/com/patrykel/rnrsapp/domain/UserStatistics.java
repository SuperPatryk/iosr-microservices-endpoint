package com.patrykel.rnrsapp.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

/**
 * Created by patryklawski on 1/12/18.
 */
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
public class UserStatistics {
  @Id
  @GeneratedValue
  private Long id;
  private Long userId;
  private Date timestamp;           // when statistic was created
  private ActivityType activityType;
  private Long timeSpentDuringDay;  // in seconds
  private Long timeSpentDuringWeek; // in seconds
  private Long timeSpentDuringMoth; // in seconds
}
