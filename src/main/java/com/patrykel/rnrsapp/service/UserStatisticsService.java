package com.patrykel.rnrsapp.service;

import com.patrykel.rnrsapp.domain.ActivityType;
import com.patrykel.rnrsapp.domain.UserStatistics;
import com.patrykel.rnrsapp.domain.UserStatisticsRepository;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Collection;
import java.util.Date;

/**
 * Created by patryklawski on 1/12/18.
 */
@Service
public class UserStatisticsService {

  private final UserStatisticsRepository userStatisticsRepository;

  public UserStatisticsService(UserStatisticsRepository userStatisticsRepository){
    this.userStatisticsRepository = userStatisticsRepository;
  }

  // GET ONE
  public UserStatistics getOne(Long userId){
    return this.userStatisticsRepository.findById(userId);
  }

  // GET ALL
  public Collection<UserStatistics> getAll(){
    return this.userStatisticsRepository.findAll();
  }

  public Collection<UserStatistics> getAll(Long userId){
    return this.userStatisticsRepository.findUserStatisticsByUserId(userId);
  }

  public Collection<UserStatistics> getAll(Long userId, ActivityType activityType){
    return this.userStatisticsRepository.findUserStatisticsByUserIdAndActivityType(userId, activityType);
  }

  public Collection<UserStatistics> getAll(Date start, Date stop){
    return this.userStatisticsRepository.findUserStatisticsByTimestampBetween(start, stop);
  }

  public Collection<UserStatistics> getAll(Long userId, Date start, Date stop){
    return this.userStatisticsRepository.findUserStatisticsByUserIdAndTimestampBetween(userId, start, stop);
  }

  public Collection<UserStatistics> getAll(Long userId, ActivityType activityType, Date start, Date stop){
    return this.userStatisticsRepository.findUserStatisticsByUserIdAndActivityTypeAndTimestampBetween(userId,
        activityType,
        start,
        stop);
  }

  public Collection<UserStatistics> getUserCurrent(Long userId){
    Date[] currentDayConstraints =  getCurrentDayConstraints();
    Date startDate = currentDayConstraints[0];
    Date stopDate = currentDayConstraints[1];
    return getAll(userId, startDate, stopDate);
  }

  public Collection<UserStatistics> getUserCurrent(Long userId, ActivityType activityType){
    Date[] currentDayConstraints =  getCurrentDayConstraints();
    Date startDate = currentDayConstraints[0];
    Date stopDate = currentDayConstraints[1];
    return getAll(userId, activityType, startDate, stopDate);
  }

  private Date[] getCurrentDayConstraints(){
    Instant instant = Instant.now();

    ZoneId zoneId = ZoneId.of( "Europe/Paris" );
    ZonedDateTime zdt = ZonedDateTime.ofInstant( instant , zoneId );

    ZonedDateTime zdtStart = zdt.toLocalDate().atStartOfDay( zoneId );
    ZonedDateTime zdtTomorrowStart = zdtStart.plusDays( 1 );

    // LocalDateT
    Date dateStart = Date.from(zdtStart.toInstant());
    Date dateEnd = Date.from(zdtTomorrowStart.toInstant());
    return new Date[]{dateStart, dateEnd};
  }

  // CREATE
  public void create(UserStatistics userStatistics) {
    this.userStatisticsRepository.save(userStatistics);
  }

  public boolean exists(UserStatistics userStatistics){
    return getOne(userStatistics.getId()) != null;
  }
}
