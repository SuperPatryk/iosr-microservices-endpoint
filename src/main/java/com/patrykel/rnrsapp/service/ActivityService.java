package com.patrykel.rnrsapp.service;

import com.patrykel.rnrsapp.domain.Activity;
import com.patrykel.rnrsapp.domain.ActivityRepository;
import com.patrykel.rnrsapp.domain.ActivityType;
import org.springframework.stereotype.Service;


import java.util.Collection;
import java.util.Date;
import java.util.stream.Collectors;

/**
 * Created by patryklawski on 1/4/18.
 */
@Service
public class ActivityService {
  private final ActivityRepository activityRepository;

  public ActivityService(ActivityRepository activityRepository) {
    this.activityRepository = activityRepository;
  }

  // GET ONE
  public Activity getOne(Long activityId){ return this.activityRepository.findById(activityId);}

  // GET ALL
  public Collection<Activity> getAll() {
    return this.activityRepository.findAll();
  }

  public Collection<Activity> getAll(Long userId) {
    return this.activityRepository.findActivitiesByUserId(userId);
  }

  public Collection<Activity> getAll(Long userId, ActivityType activityType) {
    return this.activityRepository.findActivitiesByUserIdAndActivityType(userId, activityType);
  }

  // GET ALL WITH DATE FILTERS
  public Collection<Activity> getAll(Date startDate, Date endDate) {
    return this.activityRepository.findAllByStartGreaterThanEqualAndStopLessThanEqual(
        startDate,
        endDate);
  }

  public Collection<Activity> getAll(Long userId, Date startDate, Date endDate) {
    return this.activityRepository.findActivitiesByUserIdAndStartGreaterThanEqualAndStopLessThanEqual(
        userId,
        startDate,
        endDate);
  }

  public Collection<Activity> getAll(Long userId, ActivityType activityType, Date startDate, Date endDate) {
    return this.activityRepository.findActivitiesByUserIdAndActivityTypeAndStartGreaterThanEqualAndStopLessThanEqual(
        userId,
        activityType,
        startDate,
        endDate);
  }

  private Collection<Activity> filterDates(Collection<Activity> activities, Date startDate, Date endDate) {
    return activities.stream()
        .filter(activity -> startDate.before(activity.getStart()) && endDate.after(activity.getStop()))
        .collect(Collectors.toList());
  }

  // CREATE
  public void create(Activity activity) {
    this.activityRepository.save(activity);
  }

  public boolean exists(Activity activity){
    return this.getOne(activity.getId()) != null;
  }

}
