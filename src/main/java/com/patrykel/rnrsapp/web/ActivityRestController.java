package com.patrykel.rnrsapp.web;

import com.patrykel.rnrsapp.domain.Activity;
import com.patrykel.rnrsapp.domain.ActivityType;
import com.patrykel.rnrsapp.service.ActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.Date;

/**
 * Created by patryklawski on 12/29/17.
 */

@RestController
public class ActivityRestController {

  @Autowired
  ActivityService activityService;

  // GET MAPPINGS ALL
  @GetMapping("/activity/all")
  public Collection<Activity> activitiesCollection() {
    return activityService.getAll();
  }

  @GetMapping("/activity/user/{userId}")
  public Collection<Activity> userActivitiesCollection(@PathVariable Long userId){
    return activityService.getAll(userId);
  }

  @GetMapping("/activity/user/{userId}/{activityType}")
  @ResponseBody
  public Collection<Activity> userActivitiesCollection(@PathVariable Long userId,
                                                       @PathVariable ActivityType activityType){
    return activityService.getAll(userId, activityType);
  }

  // GET MAPPINGS ALL WITH DATE FILTERS
  @GetMapping("/activity/all/filter-dates")
  public Collection<Activity> activitiesCollection(@RequestParam("startDate") Long startDate,
                                                   @RequestParam("endDate") Long endDate){
    return activityService.getAll(new Date(startDate), new Date(endDate));
  }

  @GetMapping("/activity/user/{userId}/filter-dates")
  public Collection<Activity> userActivitiesCollection(@PathVariable Long userId,
                                                       @RequestParam("startDate") Long startDate,
                                                       @RequestParam("endDate") Long endDate){
    return activityService.getAll(userId, new Date(startDate), new Date(endDate));
  }

  @GetMapping("/activity/user/{userId}/{activityType}/filter-dates")
  public Collection<Activity> userActivitiesCollection(@PathVariable Long userId,
                                                       @PathVariable ActivityType activityType,
                                                       @RequestParam("startDate") Long startDate,
                                                       @RequestParam("endDate") Long endDate) {
    return activityService.getAll(userId, activityType, new Date(startDate), new Date(endDate));
  }


  // POST MAPPINGS
  @PostMapping("/activity/create")
  public ResponseEntity<Activity> createActivity(@RequestBody Activity activity)
  {
    if (activityService.exists(activity)){
      return new ResponseEntity<>(activity, HttpStatus.CONFLICT);
    }

    activityService.create(activity);
    return new ResponseEntity<>(activity, HttpStatus.CREATED);
  }

}
