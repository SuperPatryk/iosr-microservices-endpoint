package com.patrykel.rnrsapp.web;


import com.patrykel.rnrsapp.domain.ActivityType;
import com.patrykel.rnrsapp.domain.UserStatistics;
import com.patrykel.rnrsapp.service.UserStatisticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.Date;

/**
 * Created by patryklawski on 1/12/18.
 */
@RestController
public class UserStatisticsController {

  @Autowired
  UserStatisticsService userStatisticsService;
   /*
   Endpoints:
    /user-statistics/create

    /user-statistics/all
    /user-statistics/user/{userId}
    /user-statistics/user/{userId}/{activityType}

    /user-statistics/all/filter-dates
    /user-statistics/user/{userId}/filter-dates
    /user-statistics/user/{userId}/{activityType}/filter-dates

    /user-statistics/user/{userId}/current
    /user-statistics/user/{userId}/current/{activityType}
    */

  // GET MAPPINGS ALL
  @GetMapping("/user-statistics/all")
  public Collection<UserStatistics> userStatisticsCollection() {
    return userStatisticsService.getAll();
  }

  @GetMapping("/user-statistics/user/{userId}")
  public Collection<UserStatistics> userActivitiesCollection(@PathVariable Long userId){
    return userStatisticsService.getAll(userId);
  }

  @GetMapping("/user-statistics/user/{userId}/{activityType}")
  public Collection<UserStatistics> userStatisticsCollection(@PathVariable Long userId,
                                                       @PathVariable ActivityType activityType){
    return userStatisticsService.getAll(userId, activityType);
  }

  // GET MAPPINGS ALL WITH DATE FILTERS
  @GetMapping("/user-statistics/all/filter-dates")
  public Collection<UserStatistics> userStatisticsCollection(@RequestParam("startDate") Long startDate,
                                                   @RequestParam("endDate") Long endDate){
    return userStatisticsService.getAll(new Date(startDate), new Date(endDate));
  }

  @GetMapping("/user-statistics/user/{userId}/filter-dates")
  public Collection<UserStatistics> userStatisticsCollection(@PathVariable Long userId,
                                                       @RequestParam("startDate") Long startDate,
                                                       @RequestParam("endDate") Long endDate){
    return userStatisticsService.getAll(userId, new Date(startDate), new Date(endDate));
  }

  @GetMapping("/user-statistics/user/{userId}/{activityType}/filter-dates")
  public Collection<UserStatistics> userStatisticsCollection(@PathVariable Long userId,
                                                       @PathVariable ActivityType activityType,
                                                       @RequestParam("startDate") Long startDate,
                                                       @RequestParam("endDate") Long endDate) {
    return userStatisticsService.getAll(userId, activityType, new Date(startDate), new Date(endDate));
  }

  @GetMapping("/user-statistics/user/{userId}/current")
  public Collection<UserStatistics> currentUserStatisticsCollection(@PathVariable Long userId){
    return userStatisticsService.getUserCurrent(userId);
  }

  @GetMapping("/user-statistics/user/{userId}/current/{activityType}")
  public Collection<UserStatistics> currentUserStatisticsCollection(@PathVariable Long userId,
                                                                    @PathVariable ActivityType activityType){
    return userStatisticsService.getUserCurrent(userId, activityType);
  }

  // POST MAPPINGS
  @PostMapping("/user-statistics/create")
  public ResponseEntity<UserStatistics> createActivity(@RequestBody UserStatistics userStatistics)
  {
    if(userStatisticsService.exists(userStatistics)){
      return new ResponseEntity<>(userStatistics, HttpStatus.CONFLICT);
    }

    userStatisticsService.create(userStatistics);
    return new ResponseEntity<>(userStatistics, HttpStatus.CREATED);
  }
   

}
