package com.patrykel.rnrsapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RnrsAppApplication {
  public static void main(String[] args) {
    SpringApplication.run(RnrsAppApplication.class, args);
  }
}