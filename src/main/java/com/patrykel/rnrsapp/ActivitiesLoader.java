package com.patrykel.rnrsapp;

import com.patrykel.rnrsapp.domain.Activity;
import com.patrykel.rnrsapp.domain.ActivityRepository;
import com.patrykel.rnrsapp.domain.ActivityType;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.stream.IntStream;

/**
 * Created by patryklawski on 12/29/17.
 */
@Component
@Order(1)
public class ActivitiesLoader implements CommandLineRunner {

  private ActivityRepository activityRepository;

  private final Long[] userIds = {2L, 1L, 3L, 2L, 4L};

  private final Date[] startDates = {
      new Date(),
      new Date(),
      new Date(),
      new Date(),
      new Date()
  };

  private final Date[] endDates = {
      new Date(),
      new Date(),
      new Date(),
      new Date(),
      new Date()
  };

  public ActivitiesLoader(ActivityRepository activityRepository){
    this.activityRepository = activityRepository;
  }

  @Override
  public void run(String... args) throws Exception {
    IntStream.range(0, 5)
        .forEach(i -> activityRepository.save(new Activity(
            null,
            userIds[i],
            startDates[i],
            endDates[i],
            ActivityType.randomType())));
  }
}
